<?php
/**
 * Product routes for user
 */
Route::group(['middleware' => 'auth', 'prefix' => 'product'], function(){
    Route::get('/step-1', 'Users\ProductController@productFirstStep')->name('product.step1');
    Route::post('/create', 'Users\ProductController@createProduct')->name('product.create');
});

/**
 * Profile routes for user
 */
Route::group(['middleware' => 'auth', 'prefix' => 'profile'], function(){
    Route::get('/', 'Users\ProfileController@getIndex')->name('profile.index');
});

/**
 * Seller routes for users
 */
Route::group(['middleware' => 'auth', 'prefix' => 'seller'], function(){
    // index view for seller requests
    Route::get('/request', 'Users\SellerController@getSellerRequest')->name('seller.request');

    // create a seller request
    Route::post('/request/create', 'Users\SellerController@sellerRequest')->name('seller.newRequest');
});


/**
 * Admin dashboard routes
 */
Route::group(['middleware' => ['auth','admin'], 'prefix' => 'dashboard'], function(){
    // admin index page
    Route::get('/', 'Admin\AdminController@getIndex')->name('admin.index');

    // categories group
    Route::prefix('categories')->group(function(){
        // index page for viewing category
        Route::get('/', "Admin\AdminCategoryController@getIndex")->name('admin.categories.index');

        // get all categories
        Route::get('/get-categories', 'Admin\AdminCategoryController@getCategories')->name('admin.categories.getCategories');

        // create category route
        Route::post('/create', "Admin\AdminCategoryController@createCategory")->name('admin.categories.create');

        // category edit route
        Route::get('/edit-category/{id}', 'Admin\AdminCategoryController@editCategory')->name('admin.categories.edit');

        // category update route
        Route::put('/update-category/{id}', 'Admin\AdminCategoryController@updateCategory')->name('admin.categories.update');

        // add category form view
        Route::get('/add-category-view', 'Admin\AdminCategoryController@addCategoryView')->name('admin.categories.addForm');

        // delete category route
        Route::delete('/delete-category/{id}', 'Admin\AdminCategoryController@deleteCategory')->name('admin.categories.delete');
    });

    // seller requests for admin
    Route::prefix('/seller-requests')->group(function(){
        // get the index view seller requests for admin
        Route::get('/', 'Admin\AdminSellerController@getIndex')->name('admin.seller.index');

        // get all requests of sellers
        Route::get('/get-requests', 'Admin\AdminSellerController@allRequests')->name('admin.seller.getRequests');

        // approve seller request
        Route::get('/approve-request/{id}', 'Admin\AdminSellerController@acceptRequest')->name('admin.seller.approve');

        // reject seller request
        Route::get('/reject-request/{id}', 'Admin\AdminSellerController@rejectRequest')->name('admin.seller.reject');
    });

    // All users requests for admin
    Route::prefix('/users')->group(function(){
        // get the index view for all users and sellers registered
        Route::get('/', 'Admin\AdminUsersController@index')->name('admin.users.index');

        // get all users and sellers
        Route::get('/allUsersSellers', 'Admin\AdminUsersController@allUsersSellers')->name('admin.users.allUsersSellers');

        // get all users and sellers rendered view 
        Route::get('/allUsersSeller-view', 'Admin\AdminUsersController@allUsersSellersView')->name('admin.users.usersSellersView');

        // get all sellers rendered view
        Route::get('/allsellers-view', 'Admin\AdminUsersController@allSellersView')->name('admin.users.sellersView');

        // get all sellers registered
        Route::get('/allSellers', 'Admin\AdminUsersController@allSellers')->name('admin.users.allSellers');

        Route::get('/allUsers', 'Admin\AdminUsersController@allUsers')->name('admin.users.allUsers');

        Route::get('/allUsers-view', 'Admin\AdminUsersController@allUsersView')->name('admin.users.usersView');

        Route::get('/profile/{id}', 'Admin\AdminUsersController@profile')->name('admin.users.profile');
    });
});

// guest user index view
Route::get('/', 'GuestController@welcome')->name('index');

// authentication routes
Auth::routes();

// index view for authenticated users and admin
Route::get('/home', 'HomeController@index')->name('home');

