# B2B Seller Portal
A brokerage based portal for selling and buying goods and scraps

## Project Description
This portal was designed for a single company or user who will be an intermediator between the seller and the buyer. The brokerage for selling the product will be given to the intermediator for making the deal.

### Role Descriptions:
- **Admin:**
    - Admin is the intermediator who makes the deal between the seller and the buyer.
    - Admin can view the number of products added, number of users in the system.
    - Admin can approve seller requests.
    - Admin can prioritize products as per the request of sellers
    - Admin can block a user if a report has been issued on a seller or buyer.
- **Buyer:**
    - A user registering by default will be a buyer.
    - A buyer can query about the product to the seller.
    - A buyer if wants to sell product then, buyer can request to become a seller with the proper documents.
    - A buyer can become seller on admin approval after verification.
    - A buyer can have the track record of his enquiries.
- **Seller:**
    - After the approval of admin for becoming a seller user can upload products.
    - A seller can have records of products uploaded.

## Tech Specifications
- **Front-end:**
    - HTML5
    - CSS3
    - JavaScript
    - jQuery
    - Bootstrap
- **Back-end:**
    - Laravel(php)
    - MySQL
