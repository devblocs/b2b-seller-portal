<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'mobile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * check whether the authenticated user is an admin or not
     *
     * @return boolean
     */
    public function isAdmin(){
        return $this->user_role === 'admin';
    }

    /**
     * this user has a seller profile
     *
     * @return void
     */
    public function sellerDetails(){
        return $this->hasOne('App\Model\SellerDetail');
    }

    public function fullname(){
        $fullname = ucfirst($this->name);

        return $fullname;
    }

    public function created_at(){
        return $this->created_at->diffForHumans();
    }

    public function status(){
        $status = "";

        if($this->isActive === 1){
            $status = "Active";
        }else{
            $status = "Inactive";
        }

        return $status;
    }
}
