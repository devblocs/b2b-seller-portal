<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * user is admin policy
     *
     * @param User $user
     * @return boolean
     */
    public function isAdmin(User $user){
        return $user->user_role === "admin";
    }

    /**
     * user is seller policy
     *
     * @param User $user
     * @return boolean
     */
    public function isSeller(User $user){
        return $user->user_role === "seller";
    }

    /**
     * user is user policy
     *
     * @param User $user
     * @return boolean
     */
    public function isUser(User $user){
        return $user->user_role === "user";
    }

}
