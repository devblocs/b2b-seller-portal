<?php

namespace App\Providers;

use App\User;
use App\Policies\UsersPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // define a gate for admin access
        Gate::define('admin', 'App\Policies\UserPolicy@isAdmin');

        // define a gate for seller access
        Gate::define('seller', 'App\Policies\UserPolicy@isSeller');

        // define a gate for user access
        Gate::define('user', 'App\Policies\UserPolicy@isUser');
    }
}
