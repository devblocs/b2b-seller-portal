<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SellerDetail extends Model
{
    protected $table = "seller_details";

    protected $fillable = [
        'company_name', 'id_proof', 'address', 'city', 'state', 'zip_code', 'contact_number'
    ];

    /**
     * This seller profile belongs to this user
     *
     * @return void
     */
    public function user(){
        return $this->belongsTo('App\User');
    }
}
