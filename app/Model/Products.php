<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'product_name',
        'product_description',
    ];


    public function category(){
        return $this->belongsTo('App\Model\Categories');
    }

    public function images(){
        return $this->hasMany('App\Model\ProductImage');
    }
}
