<?php

namespace App\Http\Middleware;

use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // check if authenticated is an admin or not
        if(auth()->user()->isAdmin()){
            // if authenticated proceed the next request
            return $next($request);
        }

        // else return back to home
        return redirect('/');
    }
}
