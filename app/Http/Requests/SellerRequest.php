<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SellerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required',
            'id_proof' => 'required|image',
            'address' => 'required',
            'city' => 'required|string',
            'state' => 'required|string',
            'zip_code' => 'required',
            'conact_number' => 'required|numeric'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(){
        return [
            'company_name.required' => 'Company Name is required',
            'id_proof.required' => 'Your Id Proof is required',
            'id_proof.image' => 'Id Proof must be an image',
            'address.required' => 'Company Address is required',
            'city.required' => 'City or Town Name is required',
            'city.string' => 'City or Town Name must contain only alphabets',
            'state.required' => 'State or Region Name is required',
            'state.string' => 'State or Region Name must contain only alphabets',
            'zip_code.required' => 'Your Postal code or Zip code is required',
            'conact_number.required' => 'Your Contact Number required',
            'conact_number.numeric' => 'Contact Number must contain only numbers',
        ];
    }
}
