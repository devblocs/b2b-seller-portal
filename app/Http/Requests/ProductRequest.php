<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\Limit;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name' => 'required|min:10|max:100',
            'product_description' => 'required',
            'category_id' => "required",
            'product_images.*' => ["sometimes", "required", "file","image", "mimes:jpeg,png"],
        ];
    }

    public function messages(){
        return [
            'product_name.required' => "Product Name is required",
            'product_name.min' => "Minimum 10 characters must be entered",
            'product_name.max' => "Maximum 100 characters can be entered",
            'product_description.required' => "Product Description is required",
            'category_id.required' => "Category Name is required",
            'product_images.required' => "Product images required",
            'product_images.image' => "Product image must be an image format",
            'product_images.mimes' => "Product image must be of .jpg or .png format"
        ];
    }
}
