<?php

namespace App\Http\Controllers;

use App\Model\Categories;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    /**
     * show guest user index 
     *
     * @return void
     */
    public function welcome(){
        $categories = Categories::all();

        return view('index', ['categories' => $categories]);
    }
    
}
