<?php

namespace App\Http\Controllers\Admin;

use DataTables;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminUsersController extends Controller
{
    public function index(){
        return view('admin.users.index');
    }

    public function allUsersSellers(){
        $users = User::where('user_role', '<>', 'admin')->get();

        return DataTables::of($users)
        ->addColumn('view', function($user){
            $action = "<a href='/dashboard/users/profile/". $user->id ."' class='btn btn-primary'>View Profile</a>";

            return $action;
        })
        ->editColumn('name', function($user){
            return ucfirst($user->name);
        })
        ->editColumn('isActive', function($user){
            $status = "";

            if($user->isActive === 1){
                $status = "Active";
            }else{
                $status = "Inactive";
            }

            return $status;
        })
        ->editColumn('user_role', function($user){
            return ucfirst($user->user_role);
        })
        ->editColumn('created_at', function($user){
            return $user->created_at->diffForHumans();
        })
        ->rawColumns(['view'])
        ->toJson();
    }

    public function allUsersSellersView(){
        $view = view('admin.users.allUsersSellers')->render();

        return response()->json(['view' => $view]);
    }

    public function allSellers(){
        $users = User::where('user_role', 'seller')->get();

        return DataTables::of($users)
        ->addColumn('view', function($user){
            $action = "<a href='/dashboard/users/profile/". $user->id ."' class='btn btn-primary'>View Profile</a>";

            return $action;
        })
        ->editColumn('name', function($user){
            return ucfirst($user->name);
        })
        ->editColumn('isActive', function($user){
            $status = "";

            if($user->isActive === 1){
                $status = "Active";
            }else{
                $status = "Inactive";
            }

            return $status;
        })
        ->editColumn('user_role', function($user){
            return ucfirst($user->user_role);
        })
        ->editColumn('created_at', function($user){
            return $user->created_at->diffForHumans();
        })
        ->rawColumns(['view'])
        ->toJson();
    }

    public function allSellersView(){
        $view = view('admin.users.allSellers')->render();

        return response()->json(['view' => $view]);
    }

    public function allUsers(){
        $users = User::where('user_role', 'user')->get();

        return DataTables::of($users)
        ->addColumn('view', function($user){
            $action = "<a href='/dashboard/users/profile/". $user->id ."' class='btn btn-primary'>View Profile</a>";

            return $action;
        })
        ->editColumn('name', function($user){
            return ucfirst($user->name);
        })
        ->editColumn('isActive', function($user){
            $status = "";

            if($user->isActive === 1){
                $status = "Active";
            }else{
                $status = "Inactive";
            }

            return $status;
        })
        ->editColumn('user_role', function($user){
            return ucfirst($user->user_role);
        })
        ->editColumn('created_at', function($user){
            return $user->created_at->diffForHumans();
        })
        ->rawColumns(['view'])
        ->toJson();
    }

    public function allUsersView(){
        $view = view('admin.users.allUsers')->render();

        return response()->json(['view' => $view]);
    }

    public function profile($id){

        $user = User::where('id', $id)->first();
        return view('admin.users.profile', ['user' => $user]);
    }
}
