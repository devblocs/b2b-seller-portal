<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Index view for admin dashboard
     *
     * @return view
     */
    public function getIndex(){
        return view('admin.index');
    }
}
