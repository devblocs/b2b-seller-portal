<?php

namespace App\Http\Controllers\Admin;

use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Categories; // user categories model
use Validator; // use validator facade for validation

class AdminCategoryController extends Controller
{
    /**
     * Index view for categories
     *
     * @return view Categories.index
     */
    public function getIndex(){
        // return category index view
        return view('admin.categories.index');
    }

    /**
     * add category view
     *
     * @return view addCategory
     */
    public function addCategoryView(){
        $view = view('admin.categories.addCategory')->render();

        return response()->json($view);
    }
    
    /**
     * get the list of all categories
     *
     * @return view categories/index
     */
    public function getCategories(){
        $categories = Categories::all();

        // TODO: add colspan for edit and delete
        return DataTables::of($categories)
            ->addColumn('action', function($category){
                $actions = "<div class='row'>
                <div class='col-md-6'><a href='#' class='btn btn-success update-category' data-attribute='" . $category->id . "'>Edit</a></div>
                <div class='col-md-6'>
                <form>
                    <button class='btn btn-danger delete-category' data-id='". $category->id . "' data-token='". csrf_token() ."'>Delete</button></div>
                </form>
                </div>";
                return $actions;
            })
            ->editColumn('created_at', function($category){
                return $category->created_at->diffForHumans();
            })
            ->editColumn('updated_at', function($category){
                return $category->updated_at->diffForHumans();
            })
            ->rawColumns(['action'])
            ->toJson();
        
    }

    /**
     * create a category
     *
     * @param Request $request
     * @return response $userdetails
     */
    public function createCategory(Request $request){

        // rules for saving category
        $rules = [
            'category_name' => "required|min:3|max:20"
        ];

        // validate data
        $validate_cat = $request->validate($rules);

        // if data validated then return response
        if($validate_cat){
            $category = Categories::create($request->all());

            if($category){
                return response()->json($category);
            }
        }
    }

    /**
     * edit category 
     *
     * @param integer $id
     * @return json category 
     */
    public function editCategory($id){

        $category = Categories::where('id', $id)->first();

        $view = view('admin.categories.editCategory')->render();

        return response()->json(['view' => $view, 'category' => $category]);

    }

    /**
     * update category
     *
     * @param Request $request
     * @param integer $id
     * @return json category
     */
    public function updateCategory(Request $request, $id){

        // rules for updating categories
        $rules = [
            'category_name' => "required|min:3|max:20"
        ];

        // validate data
        $validate_cat = $request->validate($rules);

        if($validate_cat){
            $category =  Categories::where('id', $id)->update(['category_name' => $request->category_name]);

            $view = view('admin.categories.addCategory')->render();

            return response()->json(['view' => $view, 'category' => $category]);
        }
    }

    /**
     * delete category
     *
     * @param interget $id
     * @return json status
     */
    public function deleteCategory($id){
        $category = Categories::find($id);

        if($category->delete()){
            return response()->json(['status' => 'success']);
        }
        
    }
}
