<?php

namespace App\Http\Controllers\Admin;

use App\User;
use DataTables;
use App\Model\SellerDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class AdminSellerController extends Controller
{
    /**
     * Index view for seller requests
     *
     * @return void
     */
    public function getIndex(){
        // index view
        return view('admin.seller.request');
    }

    /**
     * get all user requests with status pending
     *
     * @return void
     */
    public function allRequests(){
        $details = SellerDetail::where('seller_approval', 'pending')->get(); // get all requests with status pending

        // datatable json api
        return DataTables::of($details)
        ->addColumn('aprove', function($detail){
            return "<a href='#' class='btn btn-success approve-btn' data-attribute='" . $detail->id . "'>Approve</a>";
        })
        ->addColumn('reject', function($detail){
            return "<a href='#' class='btn btn-danger reject-btn' data-attribute='" . $detail->id . "'>Reject</a>";
        })
        // edit id proof column
        ->editColumn('id_proof', function($detail){
            $imagePath = asset(Storage::url($detail->id_proof));    // get the image path
            $image = "<img class='img-thumbnail ' alt='id proof' src='" . $imagePath . "' />";  // create an html element for image
            return $image; // return image

        })
        // edit created at column
        ->editColumn('created_at', function($detail){
            return $detail->created_at->diffForHumans(); // return timestamp in a human readable format
        })
        // set raw columns for parsing html elements
        ->rawColumns(['id_proof', 'aprove', 'reject'])
        ->toJson();

    }

    /**
     * accept seller request
     *
     * @param integer $id
     * @return void
     */
    public function acceptRequest($id){
        $seller = SellerDetail::find($id); //find the seller request with id

        $seller->seller_approval = 'approved'; // change approval status to approved
        $seller->seller_status = 1; // change seller status to 1

        $seller->save(); // save data

        // if seller model saved then
        if($seller){
            // find the user who sent the seller request
            // change user role to seller
            $user = User::where('id', $seller->user_id)->update([
                'user_role' => 'seller'
            ]);

            if($user){  
                return response()->json(['status' => 'approved']);
            }
        }
    }

    /**
     * reject seller request
     *
     * @param integer $id
     * @return void
     */
    public function rejectRequest($id){
        $seller = SellerDetail::where('id', $id)->update([
            'seller_approval' => 'rejected',
        ]);

        if($seller){
            return response()->json(['status' => 'rejected']);
        }
    }
}
