<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Model\Categories;
use App\Http\Controllers\Controller;
use App\Http\Requests\SellerRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class SellerController extends Controller
{

    /**
     * Seller request view
     *
     * @return void
     */
    public function getSellerRequest(){

        if( !empty(Auth::user()->sellerDetails)){
            return redirect('/');
        }

        $categories = Categories::all();
        return view('users.seller.seller', ['categories' => $categories]);
    }

    public function sellerRequest(SellerRequest $request){

        if($request->validated()){

            if($request->hasFile('id_proof')){
                // store the file in proofs folder in public folder for web access
                $path = Storage::disk('public')->putFile('proofs', $request->file('id_proof'));
            }
            
            $data =[
                'company_name' => $request->company_name,
                'id_proof' => $path,
                'address' => $request->address,
                'city' => $request->city,
                'state' => $request->state,
                'zip_code' => $request->zip_code,
                'contact_number' => $request->conact_number,
            ];

            $seller = Auth::user()->sellerDetails()->create($data);

            if($seller){
                return response()->json(['seller_details' => $seller]);
            }
        }
    }
}
