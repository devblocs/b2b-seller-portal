<?php

namespace App\Http\Controllers\users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;

class ProductController extends Controller
{
    /**
     * Add product
     *
     * @param Request $request
     * @return JSON $response
     */
    public function productFirstStep(ProductRequest $request){
        // return response()->json($request->all());

        // if request has product name, product description and category id
        if($request->has(['product_name', 'product_description', 'category_id'])){
            // validate data
            $validated = $request->validate([
                'product_name' => 'required|min:10|max:100',
                'product_description' => 'required',
                'category_id' => "required"
            ]);

            // if data validated then return status response
            if($validated){
                return response()->json(['status' => 'success']);
            }
        }
    }

    public function createProduct(ProductRequest $request){
        // return response()->json($request->all());
        
        return response()->json(['image' => gettype($request->product_images)]);
        
    }
}
