<?php

namespace App\Http\Controllers\Users;

use App\Model\Categories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function getIndex(){
        $categories = Categories::all();

        return view('users.profile.index', ['categories' => $categories]);
    }
}
