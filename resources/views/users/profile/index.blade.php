@extends('layouts.user')

@section('title', Auth::user()->fullname() . " | Profile")

@section('content')

<section class="user-profile-section section-t8">
    <div class="container">
        <div class="row">
            <div class="col-3">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link my-navy active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home"
                        role="tab" aria-controls="v-pills-home" aria-selected="true">My Profile</a>
                    <a class="nav-link my-navy" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile"
                        role="tab" aria-controls="v-pills-profile" aria-selected="false">My Enquiry</a>
                    <a class="nav-link my-navy" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages"
                        role="tab" aria-controls="v-pills-messages" aria-selected="false">My Product</a>

                </div>
            </div>
            <div class="col-9">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                        <form class="email-login form-a contactForm">
                            <div class="row">
                                <div class="col-md-7 mb-1">
                                    <div class="form-group margin-1">
                                        <input type="text" name="name" class="form-control form-control-lg form-control-a"
                                            placeholder="John Mayer" data-rule="minlen:4" data-msg="Please enter at your registered email">
                                        <div class="validation"></div>
                                    </div>
                                </div>
                                <div class="col-md-7 mb-1">
                                    <div class="form-group margin-1">
                                        <input type="text" name="name" class="form-control form-control-lg form-control-a"
                                            placeholder="JohnMayer_98" data-rule="minlen:4" data-msg="Please enter at your registered email">
                                        <div class="validation"></div>
                                    </div>
                                </div>
                                <div class="col-md-7 mb-1 margin-1">
                                    <div class="form-group">
                                        <input type="password" name="name" class="form-control form-control-lg form-control-a"
                                            placeholder="Your Password" data-rule="minlen:4" data-msg="Please enter Password">
                                        <div class="validation"></div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <button type="submit" class="btn btn-a">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="my-inner-box">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-filter" data-target="Chemicals">Chemicals</button>
                                                        <button type="button" class="btn btn-filter" data-target="Metal">Metal</button>
                                                        <button type="button" class="btn btn-filter" data-target="Polymer">Polymer</button>
                                                        <button type="button" class="btn btn-filter active-tab"
                                                            data-target="all">All</button>
                                                    </div>
                                                </div>
                                                <div class="table-container">
                                                    <table class="table table-filter">
                                                        <tbody>
                                                            <tr data-status="Chemicals">

                                                                <td>
                                                                    <div class="media">
                                                                        <a href="#" class="pull-left">
                                                                            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg"
                                                                                class="media-photo">
                                                                        </a>
                                                                        <div class="media-body">
                                                                            <span class="media-meta pull-right">Febrero
                                                                                13, 2016</span>
                                                                            <h4 class="title">
                                                                                Lorem Impsum
                                                                                <span class="pull-right Chemicals">(Chemicals)</span>
                                                                            </h4>
                                                                            <p class="summary">Ut enim ad minim
                                                                                veniam, quis nostrud
                                                                                exercitation...</p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr data-status="Metal">

                                                                <td>
                                                                    <div class="media">
                                                                        <a href="#" class="pull-left">
                                                                            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg"
                                                                                class="media-photo">
                                                                        </a>
                                                                        <div class="media-body">
                                                                            <span class="media-meta pull-right">Febrero
                                                                                13, 2016</span>
                                                                            <h4 class="title">
                                                                                Lorem Impsum
                                                                                <span class="pull-right Metal">(Metal)</span>
                                                                            </h4>
                                                                            <p class="summary">Ut enim ad minim
                                                                                veniam, quis nostrud
                                                                                exercitation...</p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr data-status="Polymer">

                                                                <td>
                                                                    <div class="media">
                                                                        <a href="#" class="pull-left">
                                                                            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg"
                                                                                class="media-photo">
                                                                        </a>
                                                                        <div class="media-body">
                                                                            <span class="media-meta pull-right">Febrero
                                                                                13, 2016</span>
                                                                            <h4 class="title">
                                                                                Lorem Impsum
                                                                                <span class="pull-right Polymer">(Polymer)</span>
                                                                            </h4>
                                                                            <p class="summary">Ut enim ad minim
                                                                                veniam, quis nostrud
                                                                                exercitation...</p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                        <div class="container">
                            <div class="add-pro-btn">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-a" data-toggle="modal"
                                                    data-target="#add-products">ADD PRODUCT</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="added-pro">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <div class="pro-detail">
                                                    <img src="img/author-1.jpg" alt="#">
                                                    <div class="pro-description">
                                                        <h5>Lorem, ipsum dolor.</h5>
                                                        <p class="pro-des-mar">Lorem ipsum dolor sit amet.</p>
                                                        <p class="pro-des-mar">Lorem ipsum dolor sit amet.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 flex-end">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-a" data-toggle="modal"
                                                            data-target="#edit-products">Edit</button>
                                                        <div class="modal fade" id="edit-products" tabindex="-1"
                                                            role="dialog" aria-labelledby="edit-productsTitle"
                                                            aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="w-title-a text-brand">Edit
                                                                            Products</h5>
                                                                        <button type="button" class="close"
                                                                            data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <form class="form-a contactForm"
                                                                                    action="" method="post" role="form">
                                                                                    <div id="errormessage"></div>
                                                                                    <div class="row">

                                                                                        <div class="col-md-12">
                                                                                            <div class="form-group">
                                                                                                <label class="btn btn-a">
                                                                                                    Choose Image
                                                                                                    <input class="file-input"
                                                                                                        type="file" size="60" multiple value="SELECT IMAGES">
                                                                                                </label>

                                                                                                <div class="preview"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class="form-group">
                                                                                                <input name="text"
                                                                                                    type="email"
                                                                                                    class="form-control form-control-lg form-control-a"
                                                                                                    placeholder="Product Name"
                                                                                                    data-rule="email"
                                                                                                    data-msg="Please enter a City/Town">
                                                                                                <div class="validation"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class="form-group">
                                                                                                <input type="text"
                                                                                                    name="name"
                                                                                                    class="form-control form-control-lg form-control-a"
                                                                                                    placeholder="Product Description"
                                                                                                    data-rule="minlen:4"
                                                                                                    data-msg="Please enter at least 4 chars">
                                                                                                <div class="validation"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class="form-group">
                                                                                                <select class="form-control form-control-lg form-control-a"
                                                                                                    id="sel1">
                                                                                                    <option>Select
                                                                                                        Category</option>
                                                                                                    <option>2</option>
                                                                                                    <option>3</option>
                                                                                                    <option>4</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-a"
                                                                            data-dismiss="modal">Close</button>
                                                                        <button type="button" class="btn btn-a">Save
                                                                            changes</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <button type="submit" class="btn btn-a" data-toggle="modal"
                                                            data-target="#Delete-pro">Delete</button>
                                                        <div class="modal fade" id="Delete-pro" tabindex="-1" role="dialog"
                                                            aria-labelledby="Delete-proTitle" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="w-title-a text-brand">Delete
                                                                            Product</h5>
                                                                        <button type="button" class="close"
                                                                            data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        DELETE THIS PRODUCT
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-a"
                                                                            data-dismiss="modal">Cancel</button>
                                                                        <button type="button" class="btn btn-a">Delete</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
    
@endsection