@extends('layouts.user')

@section('title', 'Become a seller')

@section('content')
<section class="intro-single">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-lg-8">
              <div class="title-single-box">
                <h1 class="title-single">Become a Seller</h1>
              </div>
            </div>
            <div class="col-md-12 col-lg-4">
              <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="/">Home</a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">
                    Become a Seller
                  </li>
                </ol>
              </nav>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 section-t3">
              <form class="form-a contactForm" id="seller-form" method="POST" enctype="multipart/form-data">
                @csrf
                <div id="errormessage"></div>
                <div class="row">
                  <div class="col-md-8 mb-3">
                    <div class="form-group" id="company_name_block">
                      <input type="text" name="company_name" id="company_name" class="form-control form-control-lg form-control-a" placeholder="Enter Your Company Name">
                        <span class="help-block" id="company_name_help"></span>
                    </div>
                  </div>
                  <div class="col-md-8 mb-3">
                    <div class="form-group" id="id_proof_block">
                        <input type="file" name="id_proof" id="id_proof" placeholder="Upload ID Proof" class="f-input form-control form-control-lg form-control-a" />
                      <span class="help-block" id="id_proof_help"></span>
                    </div>
                  </div>
                  <div class="col-md-8 mb-3">
                      <div class="form-group" id="address_block">
                          <textarea name="address" class="form-control" id="address"  cols="45" rows="5" placeholder="Enter your Company Address"></textarea>
                            <span class="help-block" id="address_help"></span>
                      </div>
                  </div>
                  <div class="col-md-8 mb-3">
                    <div class="form-group" id="city_block">
                        <input type="text" name="city" id="city" class="form-control form-control-lg form-control-a" placeholder="Enter Your City / Town Name">
                          <span class="help-block" id="city_help"></span>
                    </div>
                  </div>
                  <div class="col-md-8 mb-3">
                    <div class="form-group" id="state_block">
                        <input type="text" name="state" id="state" class="form-control form-control-lg form-control-a" placeholder="Enter Your State / Region Name">
                          <span class="help-block" id="state_help"></span>
                    </div>
                  </div>
                  <div class="col-md-8 mb-3">
                      <div class="form-group" id="zip_code_block">
                          <input type="text" name="zip_code" id="zip_code" class="form-control form-control-lg form-control-a" placeholder="Enter Your Postal / Zipcode">
                            <span class="help-block" id="zip_code_help"></span>
                      </div>
                  </div>
                  <div class="col-md-8 mb-3">
                      <div class="form-group" id="conact_number_block">
                          <input type="text" name="conact_number" id="conact_number" class="form-control form-control-lg form-control-a" placeholder="Enter Your Conact Number">
                            <span class="help-block" id="conact_number_help"></span>
                      </div>
                  </div>
    
                  <div class="col-md-12">
                    <button class="btn btn-a">Become a Seller</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>

@endsection

@section('script')
  <script src="{{ asset('js/dropzone.js') }}"></script>

  <script>
    $(document).ready(function(){

      var baseurl = "{{ url('/') }}"; // get baseurl

      // on seller request form submission
      $(document).on('submit', '#seller-form', function(e){
        e.preventDefault();

        $.ajax({
          url: baseurl + "/seller/request/create",
          method: 'POST',
          data: new FormData($('#seller-form')[0]),
          dataType: 'JSON',
          contentType: false,
          cache: false,
          processData: false,
          success: function(data){
            $('.form-group').removeClass('has-error');
            $('.help-block').empty();

            $('#seller-form')[0].reset();

            swal({
              title: "Voila!",
              text: "Your request submitted successfully, We will notify you soon.",
              icon: "success",
              buttons: false,
              timer: 3000
            }).then(() => {
              window.location = baseurl;
            });

            // iziToast.success({
            //     title: 'Voila!',
            //     message: 'Your request submitted successfully',
            //     position: 'topRight',
            //     icon: 'fa fa-check',
            // });

          },
          error: function(error){

            if(error.status === 422){
              
              var form_errors = error.responseJSON.errors;

              $.each(form_errors, function(key, value){
                  $('#' + key + "_block").addClass('has-error');

                  $('#' + key + "_help").empty();
                  $('#' + key + "_help").append(value);
              });
            }
          }
        });
      });
    });
       
  </script>
@endsection