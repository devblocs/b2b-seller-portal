<!DOCTYPE html>
<html lang="en">
    @include('partials.user._head')
<body>
 @include('partials.user._navbar')
  
    @yield('content')

    @auth
    <div class="modal fade" id="add-products" tabindex="-1" role="dialog" aria-labelledby="add-productsTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h3 class="panel-title">Upload product in quick 2 steps!</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>    
                    </div>
                    <!-- Modal Header ends -->

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="panel-body">
                                <form name="basicform" enctype="multipart/form-data" id="product_form" method="post">
                                    @csrf
                                <!-- form 1 -->
                                    <div id="step1" class="product-detail">
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-md-7">
                                                    <legend class="text-right legend-title">Step 1 of 2</legend>
                                            </div>
                                            <div class="col-md-5 text-right refresh-block">
                                                    <a href="#" class="refresh" title="Reset Data"><i class="fa fa-refresh" aria-hidden="true"></i></a> 
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input name="product_name" id="product_name" type="text" class="form-control form-control-lg form-control-a" placeholder="Product Name">
                                                    <div class="validation" id="product_name_error"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea name="product_description" id="product_description" cols="37" class="form-control form-control-lg form-control-a" rows="8" placeholder="Product Description"></textarea>
                                                    <div class="validation" id="product_description_error"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <select class="form-control form-control-lg form-control-a" id="category_id" name="category_id" >
                                                        <option value="">Select Category</option>
                                                        @if(count($categories) == 0)
                                                            <option>No categories</option>
                                                        @else
                                                            @foreach ($categories as $category)
                                                                <option value="{{ $category->id }}">{{ ucfirst($category->category_name) }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                    <div class="validation" id="category_id_error"></div>
                                                </div>
                                            </div>
                                        </div>
                                        
                            
                                        <div class="modal-footer">
                                            <div class="container text-center">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12">
                                                        <button type="button" class="btn btn-a" data-dismiss="modal">Close</button>
                                                            
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <button class="btn btn-a" id="next" type="button">Next <span class="fa fa-arrow-right"></span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            
                                    </fieldset>
                                    </div>

                                    <!-- form 2 -->
                                <div id="step2" class="product-detail" style="display: none;">
                                <fieldset>
                                        <div class="row">
                                            <div class="col-md-7">
                                                    <legend class="text-right legend-title">Step 2 of 2</legend>
                                            </div>
                                            <div class="col-md-5 text-right refresh-block">
                                                    <a href="#" class="refresh" title="Reset Data"><i class="fa fa-refresh" aria-hidden="true"></i></a> 
                                            </div>
                                        </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p id="product-image-title">Upload Product Images <span id="product-image-notice">(select upto 5 images)</span></p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <div class="form-group">
                                                <label class="btn btn-a">
                                                    Choose Image
                                                    <input class="file-input" name="product_images[]" id="product-image" type="file" size="60" multiple>
                                                </label>

                                                <div class="preview-image"></div>
                                            </div>
                                        </div>
                                    </div>

                            <!-- Modal Footer -->
                                <div class="modal-footer">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <button class="btn btn-a" id="back" type="button"><span class="fa fa-arrow-left"></span> Back</button> 
                                                    
                                                </div>
                                                <div class="col-md-6">
                                                        <button type="button" id="add_product" class="btn btn-a">Add Product</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        
                                </fieldset>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endauth

 @include('partials.user._footer')

  @include('partials.user._footer_scripts')

</body>
</html>
