<!DOCTYPE html>
<html>
@include('partials.admin._head')
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    @include('partials.admin._top_navbar')
  
    @include('partials.admin._sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->

  @include('partials.admin._footer')

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

@include('partials.admin._footer_scripts')
</body>
</html>
