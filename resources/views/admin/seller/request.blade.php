@extends('layouts.admin')

@section('title', 'Seller Requests')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Seller Requests
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div id="table-box" class="box">
                    <div class="box-header">
                        <h3 class="box-title" id="box-message">All Seller Requests</h3>
                    </div>

                    <div class="box-body" id="request-body">
                        <table id="requests-table" class="table table-bordered table-striped table-responsive details-table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Company Name</th>
                                <th>ID Proof</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Zip Code</th>
                                <th>Contact Number</th>
                                <th>Created At</th>
                                <th class="text-center" colspan="2">Actions</th>
                                <th class="hidden-column"></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>                                   
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>

        $(document).ready(function(){
            var baseurl = "{{ url('/') }}";

            var table = $('#requests-table').DataTable({
                serverSide: true,
                ajax: '{{ route("admin.seller.getRequests") }}',
                columns: [
                    // getting the datas from the datatable query
                    {data: 'id', name: 'id'},
                    {data: 'company_name', name: 'company_name'},
                    {data: 'id_proof', name: 'id_proof', orderable: false, searchable: false},
                    {data: 'address', name: 'address', orderable: false},
                    {data: 'city', name: 'city'},
                    {data: 'state', name: 'state'},
                    {data: 'zip_code', name: 'zip_code', orderable: false},
                    {data: 'contact_number', name: 'contact_number', orderable: false},
                    {data: 'created_at', name: 'created_at', searchable: false},
                    {data: 'aprove', name: 'aprove', orderable: false, searchable: false},
                    {data: 'reject', name: 'reject', orderable: false, searchable: false},
                ]
            });

            $(document).on('click', '.approve-btn', function(e){
                e.preventDefault();
                
                var requestId = $(this).attr('data-attribute');
                $.ajax({
                    url: baseurl + '/dashboard/seller-requests/approve-request/' + requestId,
                    type: 'GET',
                    dataType: 'json',
                    success: function(data){

                        // ISSUES: table not reloading on successfull approval
                        // TODO: complete table reload
                        $('#requests-table').DataTable().ajax.reload();
                    }
                });
            });


            $(document).on('click', '.reject-btn', function(e){

                var sellerId = $(this).attr('data-attribute');

                $.ajax({
                    url: baseurl + '/dashboard/seller-requests/reject-request/' + sellerId,
                    type: 'GET',
                    dataType: 'json',
                    success: function(data){

                        // ISSUES: table not reloading on successfull rejection
                        // TODO: complete table reload
                        table.ajax.reload();

                    }
                });
            });

        });
    </script>
@endsection