<div class="box-header with-border">
    <h3 class="box-title">All Sellers</h3>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="table-responsive">
            <table class="table table-bordered table-striped details-table" id="all_sellers_table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>User Status</th>
                            <th>User Role</th>
                            <th>Created At</th>
                            <th class="text-center">Actions</th>
                            <th class="hidden-column"></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>      
                </table>
      </div>
  </div>
  <!-- /.box-body -->

  <script>

      // datatable for all sellers table
      var sellersTable = $('#all_sellers_table').DataTable({
              ServerSide: true,
              ajax: "{{ route('admin.users.allSellers') }}",
              columns: [
                  {data: "id", name: 'id'},
                  {data: "name", name: 'name'},
                  {data: "email", name: 'email'},
                  {data: "mobile", name: 'mobile'},
                  {data: "isActive", name: 'isActive'},
                  {data: "user_role", name: 'user_role'},
                  {data: "created_at", name: 'created_at', orderable: false, searchable: false},
                  {data: "view", name: 'view', orderable: false, searchable: false},
              ]
            });
  </script>