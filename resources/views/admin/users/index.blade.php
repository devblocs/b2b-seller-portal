@extends('layouts.admin')

@section('title', 'All Users and Sellers')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        All Users and Sellers
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
      <div class="col-md-3">
        {{-- <div class="box box-solid">
          <div class="box-header with-border">
            <h3 class="box-title">Folders</h3>
          </div>
          <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
              <li class="active"><a href="#"><i class="fa fa-inbox"></i> Inbox
                <span class="label label-primary pull-right">12</span></a></li>
              <li><a href="#"><i class="fa fa-envelope-o"></i> Sent</a></li>
              <li><a href="#"><i class="fa fa-file-text-o"></i> Drafts</a></li>
              <li><a href="#"><i class="fa fa-filter"></i> Junk <span class="label label-warning pull-right">65</span></a>
              </li>
              <li><a href="#"><i class="fa fa-trash-o"></i> Trash</a></li>
            </ul>
          </div>
          <!-- /.box-body -->
        </div> --}}
        <!-- /. box -->
        <div class="box box-solid">
          <div class="box-header with-border">
            <h3 class="box-title">Filter</h3>
          </div>
          <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
              <li><a href="#" id="sellers_users"><i class="fa fa-circle-o text-red"></i>Users & Sellers</a></li>
              <li><a href="#" id="all_sellers"><i class="fa fa-circle-o text-yellow"></i>All Sellers</a></li>
              <li><a href="#" id="all_users"><i class="fa fa-circle-o text-light-blue"></i>All Users</a></li>
            </ul>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="box box-primary" id="table-body">
          <div class="box-header with-border">
            <h3 class="box-title">All users and Sellers</h3>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
              <div class="table-responsive">
                    <table class="table table-bordered table-striped details-table" id="all_sellers_users">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>User Status</th>
                                    <th>User Role</th>
                                    <th>Created At</th>
                                    <th class="text-center">Actions</th>
                                    <th class="hidden-column"></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>      
                        </table>
              </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /. box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
    
@endsection

@section('scripts')

    <script>
      // on document load
        $(document).ready(function(){
            var baseurl = "{{ url('/') }}"; // get the base url

            // set datatable for the all users and seller table
            var usersTable = $('#all_sellers_users').DataTable({
                ServerSide: true,
                ajax: "{{ route('admin.users.allUsersSellers') }}",
                columns:[
                    {data: "id", name: 'id'},
                    {data: "name", name: 'name'},
                    {data: "email", name: 'email'},
                    {data: "mobile", name: 'mobile'},
                    {data: "isActive", name: 'isActive'},
                    {data: "user_role", name: 'user_role'},
                    {data: "created_at", name: 'created_at', orderable: false, searchable: false},
                    {data: "view", name: 'view', orderable: false, searchable: false},
                ]
            });


            // on selection of all sellers filter
            $(document).on('click', '#all_sellers', function(){
              $.ajax({
                url: "{{ route('admin.users.sellersView') }}",
                type: 'GET',
                dataType: 'json',
                success: function(data){
                    $('#table-body').empty();

                    $('#table-body').append(data.view);
                },
              });
            });

            // on selection of all users and sellers
            $(document).on('click', '#sellers_users', function(){
              $.ajax({
                url: "{{ route('admin.users.usersSellersView') }}",
                type: 'GET',
                dataType: 'json',
                success: function(data){
                  $('#table-body').empty();

                  $('#table-body').append(data.view);
                }
              });
            });

            $(document).on('click', '#all_users', function(){
              $.ajax({
                url: "{{ route('admin.users.usersView') }}",
                type: 'GET',
                dataType: 'json',
                success: function(data){
                  $('#table-body').empty();

                  $('#table-body').append(data.view);
                }
              });
            });
        });
    </script>
    
@endsection

