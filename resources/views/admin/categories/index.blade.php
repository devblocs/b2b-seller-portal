@extends('layouts.admin')

@section('title', 'Categories')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Categories
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary" id="category-forms">
                    <div class="box-header with-border">
                    <h3 class="box-title" id="form-title">Add Category</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form id="create_category" role="form" method="POST">
                    <div class="box-body">
                        @csrf
                        <div id="category" class="form-group">
                        <label id="category_label" for="category_name">Category Name</label>
                        
                        <input type="text" class="form-control clear_form" id="category_name" name="category_name" placeholder="Enter category name">
                        <span class="help-block" id="category_name_help"></span>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <input class="btn btn-primary" type="submit" name="submit" id="submit" value="Create Category">
                    </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-6">
                <div id="table-box" class="box">
                    <div class="box-header">
                            <h3 class="box-title" id="box-message">All Categories</h3>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body" id="category_table_box">
                            <table id="category_table" class="table table-bordered table-striped table-responsive details-table">
                                <thead>
                                <tr>
                                    <th id="sample">Id</th>
                                    <th>Category Name</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>                                   
                            </table>
                    </div>
                    <!-- /.box-body --> 
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection

@section('scripts')

    <script>
            // on document ready
            $(document).ready(function(){
                 
                var baseurl = "{{ url('/') }}"; // get the base url of the website

                // activate data table
                var table = $('#category_table').DataTable({
                        serverSide: true,
                        ajax: '{{ route("admin.categories.getCategories") }}',
                        columns: [
                            // getting the datas from the datatable query
                            {data: 'id', name: 'id'},
                            {data: 'category_name', name: 'category_name'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'updated_at', name: 'updated_at'},
                            {data: 'action', name: 'action', orderable: false, searchable: false},
                        ]
                });


                // add category form view on update cancellation
                $(document).on('click', "#cancel-update", function(){
                    $.ajax({
                        url: "{{ route('admin.categories.addForm') }}",
                        type: "GET",
                        dataType: 'json',
                        success: function(data){
                            $('#category-forms').empty(); // clear the form box

                            $('#category-forms').append(data); // append the rendered data
                        }
                    });
                });

                // on add category form submission
                $(document).on('submit', "#create_category" , function(e){
                    e.preventDefault(); // prevent default action
                    $.ajax({
                            url: baseurl + '/dashboard/categories/create', // url to send request
                            method: 'POST', // request method
                            data: {
                                '_token': $('input[name="_token"]').val(), // token value
                                'category_name': $('#category_name').val() // category value
                            },
                            success: function(data){
                                /* on successfull data response */

                                $('#category').removeClass('has-error'); // remove has-error class
                                $('.help-block').empty(); // clear the error message

                                $('#create_category')[0].reset(); // clear all form inputs 

                                $('#no-category').empty(); 

                                // reload the table
                                setTimeout(function(){
                                    table.ajax.reload();
                                });                                
                            },
                            error: (data) => {
                                /* if any error exits */

                                // check if the error status is 422, validation error
                                if(data.status === 422){

                                    $('#category').addClass('has-error'); //add error class to div

                                    var val_errors = data.responseJSON.errors; // store all the errors in a variable

                                    // iterate through errors
                                    $.each(val_errors, (key, val) => {

                                        $("#" + key + "_help").empty(); // clear previous error message appended
                                        $("#" + key + "_help").append(val); // add the error messages to the message block 
                                    });  
                                }
                            }
                        });
                    });


                    // edit category to retrieve data
                    $(document).on('click', '.update-category', function(e){
                        e.preventDefault();

                        var cat_id = $(this).attr('data-attribute'); // Get category id

                        $.ajax({
                            url: baseurl + '/dashboard/categories/edit-category/' + cat_id,
                            type: 'GET',
                            dataType: 'json',
                            success: function(data){
                                $('#category-forms').empty(); // clear category forms box

                                $('#category-forms').append(data.view); // append the the form

                                $('#cat_id').val(data.category.id); // add the category id to input field
                                $('#category_name').val(data.category.category_name); // add category name to input field
                            }
                        });
                    });

                    // on submission of updated data
                    $(document).on('submit', '#update_category', function(e){
                        e.preventDefault();

                        var u_cat_id = $('#cat_id').val(); // get category name

                        $.ajax({
                            url: baseurl + '/dashboard/categories/update-category/' + u_cat_id,
                            method: 'POST',
                            data: {
                                '_token': $('[name="_token"]').val(),
                                '_method': $('[name="_method"]').val(),
                                'cat_id': $('#cat_id').val(),
                                'category_name': $('#category_name').val()
                            },
                            success: function(data){
                                
                                $('#category-forms').empty(); // clear the forms box

                                $('#category-forms').append(data.view); // append 

                                // reload the table
                                setTimeout(function(){
                                    table.ajax.reload();
                                });
                            },
                            error: function(error){
                                /* if any error exits */

                                // check if the error status is 422, validation error
                                if(error.status === 422){

                                    $('#category').addClass('has-error'); //add error class to div

                                    var val_errors = error.responseJSON.errors; // store all the errors in a variable

                                    // iterate through errors
                                    $.each(val_errors, (key, val) => {

                                        $("#" + key + "_help").empty(); // clear previous error message appended
                                        $("#" + key + "_help").append(val); // add the error messages to the message block 
                                    });

                                }
                            }
                        });                        
                    });

                    // delete category
                    $(document).on('click', '.delete-category', function(e){
                        e.preventDefault();

                        var del_cat_id = $(this).attr('data-id'); // get category id
                        var token = $(this).attr('data-token'); // get token

                        // create a sweet alert
                        swal({
                            title: "Are you sure?",
                            text: "Category deleted cannot be recovered!",
                            icon: "warning",
                            buttons: {
                                cancel: "Cancel",
                                delete: {
                                    text: "Delete!",
                                    value: 'delete'
                                },
                            },
                        }).then(function(value){
                            // if delete button clicked
                            if(value === "delete"){
                                $.ajax({
                                    url: baseurl + "/dashboard/categories/delete-category/" + del_cat_id,
                                    type: 'POST',
                                    dataType: 'json',
                                    data: {
                                        '_token': token,
                                        '_method': "DELETE",
                                        'id': del_cat_id
                                    },
                                    success: function(data){
                                        // if status success
                                        if(data.status === "success");{
                                            // activate delete successfull message
                                            swal({
                                                title: "Done!",
                                                text: "Category deleted successfully!",
                                                icon: "success",
                                                buttons: false,
                                                timer: 1100
                                            });

                                            // reload table
                                            setTimeout(function(){
                                                table.ajax.reload();
                                            });
                                        }
                                    }
                                });

                            }else{
                                swal.stopLoading(); // stop loading all sweet alerts
                                swal.close(); // close sweet alert modal
                            }
                        });
                    });

            });
          </script>
@endsection

