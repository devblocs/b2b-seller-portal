    <div class="box-header with-border">
    <h3 class="box-title" id="form-title">Add Category</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form id="create_category" role="form" method="POST">
    <div class="box-body">
        @csrf
        <div id="category" class="form-group">
        <label id="category_label" for="category_name">Category Name</label>
        
        <input type="text" class="form-control clear_form" id="category_name" name="category_name" placeholder="Enter category name">
        <span class="help-block" id="category_name_help"></span>
        </div>
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        <input class="btn btn-primary" type="submit" name="submit" id="submit" value="Create Category">
    </div>
    </form>