<div class="box-header with-border">
    <h3 class="box-title" id="form-title">Update Category</h3>
</div>
<!-- /.box-header -->
 <!-- form start -->
 <form id="update_category" role="form" method="POST">
        <div class="box-body">
            @csrf
            @method('PUT')
            <input type="hidden" id="cat_id" name="cat_id">
            <div id="category" class="form-group">
            <label id="category_label" for="category_name">Category Name</label>
            
            <input type="text" class="form-control clear_form" id="category_name" name="category_name" placeholder="Enter category name">
            <span class="help-block" id="category_name_help"></span>
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <div class="row text-center">
                <div class="col-md-6">
                    <input class="btn btn-primary" type="submit" name="submit" id="submit" value="Update Category">
                </div>
                <div class="col-md-6">
                    <a href="#" class="btn btn-danger" id="cancel-update">Cancel</a>
                </div>
            </div>
            
        </div>
    </form>