<!-- JavaScript Libraries -->
<script src="{{ asset('users/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('users/lib/jquery/jquery-migrate.min.js') }}"></script>
<script src="{{ asset('users/lib/popper/popper.min.js') }}"></script>
<script src="{{ asset('users/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('users/lib/easing/easing.min.js') }}"></script>
<script src="{{ asset('users/lib/owlcarousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('users/lib/scrollreveal/scrollreveal.min.js') }}"></script>
<script src="{{ asset('users/js/iziToast.min.js') }}"></script>

<!-- plugins File -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Contact Form JavaScript File -->
{{-- <script src="{{ asset('users/contactform/contactform.js') }}"></script> --}}

<!-- Template Main Javascript File -->
<script src="{{ asset('users/js/main.js') }}"></script>

<script>
        // on document ready
        $(document).ready(function(){
            var product_form = $('#product_form'); // product form

            var baseurl = "{{ url('/') }}"; // base url

            // on mouse hover over reset button
            $('.refresh').mouseover(function(e){
                e.preventDefault();

                $('.fa-refresh').addClass('fa-spin'); // add spin class to icon
            });

            // on mouse remove over reset button
            $('.refresh').mouseout(function(e){
                e.preventDefault();

                $('.fa-refresh').removeClass('fa-spin'); // remove spin class from icon
            });

            // on click of reset button
            $('.refresh').click(function(e){
                e.preventDefault();

                product_form[0].reset(); // reset form
                tinymce.get('product_description').setContent(''); // clear tinyMCE textarea
                $('.validation').empty(); // clear validation errors
            });

            // on click of next button
            $('#next').click(function(e){
                e.preventDefault();
                $('#step1').hide(); // hide step 1 form

                // send ajax request for validating data
                $.ajax({
                    url: baseurl + '/product/step-1',
                    method: "GET",
                    data: {
                        "product_name" : $('#product_name').val(),
                        'product_description' : tinymce.get('product_description').getContent(),
                        'category_id' : $('#category_id').val()
                    },
                    success: function(data){
                        // console.log(data);

                        // if status success
                        if(data.status == 'success'){
                            $('.validation').empty(); // clear all validation errors
                            $('#step2').toggle('slide'); // display step 2 form
                        }
                    },
                    error: function(data){
                        // if status response code is 422
                        if(data.status === 422){
                            var errors = data.responseJSON.errors; // collect all errors

                            $('.validation').empty(); // clear existing errors messages
                            $('#step2').hide(); // hide step 2 form
                            $('#step1').show();  // show step 1 form with errors
                            $('.validation').addClass('form-error'); // add validation class for formatting

                            // iterate through errors 
                            $.each(errors, (key, value) => {
                                $('#' + key + '_error').append(value); // append error messages to error block  
                            });
                        }
                    }
                });  
            });

            $(document).on('click', '#add_product', function(e){
                e.preventDefault();
                var fd = new FormData($('#product_form')[0]);
                var fileLength = document.getElementById('product-image').files.length;

                // fd.append('_token', "{{ csrf_token() }}");
                // fd.append('product_name', $('#product_name').val());
                // fd.append('product_description', tinymce.get('product_description').getContent());
                // fd.append('category_id', $('#category_id').val());

                // for(var x = 0; x < fileLength; x++){
                //     fd.append('product_images[]', document.getElementById('product-image').files[x]);
                // }

                

                // console.log(fd);
                $.ajax({
                    url: baseurl + '/product/create',
                    method: "POST",
                    data: fd,
                    dataType: "JSON",
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data){
                        console.log(data);
                    }
                });
            });

            // on click of back button
            $('#back').click(function(){
                $('#step2').hide(); // hide step 2 form
                $('#step1').toggle('slide'); // display step 1 form
            });

            $('#product-image').on("change", previewImages); // show image preview on upload

        });
    </script>

@yield('script')