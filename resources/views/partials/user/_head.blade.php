<head>
    <meta charset="utf-8">
    <title>B2B Seller | @yield('title')</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
  
    <!-- Favicons -->
    {{-- <link href="{{ asset('users/img/logo.png') }}" rel="icon">
    <link href="{{ asset('users/img/logo.png') }}" rel="apple-touch-icon"> --}}
  
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
  
    <!-- Bootstrap CSS File -->
    <link href="{{ asset('users/lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  
    <!-- Libraries CSS Files -->
    <link href="{{ asset('users/lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('users/lib/animate/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('users/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('users/css/iziToast.min.css') }}">
    
  
    <!-- Main Stylesheet File -->
    <link href="{{ asset('users/css/style.css') }}" rel="stylesheet">

    <!-- external js File -->
    <script src="{{ asset('users/lib/tinymce/tinymce.min.js') }}"></script>

    <script>
        // initialize tinymce
        tinymce.init({
            selector: '#product_description',
            height: 300,
            menubar: false,
            plugins: [
              'advlist autolink lists link image charmap print preview anchor',
              'searchreplace visualblocks code fullscreen',
              'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat',
            branding: false
        });
    </script>
  
    @yield('head-script')
  </head>