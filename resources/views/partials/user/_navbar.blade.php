<div class="click-closed"></div>
<!--/ Form Search Star /-->
<div class="box-collapse">
  <div class="title-box-d">
    <h3 class="title-d">Search</h3>
  </div>
  <span class="close-box-collapse right-boxed ion-ios-close"></span>
  <div class="box-collapse-wrap form">
    <form class="form-a">
      <div class="row">
        <div class="col-md-6 mb-2">
          <div class="form-group">
            <label for="Type">Keyword</label>
            <input type="text" class="form-control form-control-lg form-control-a" placeholder="Keyword">
          </div>
        </div>
        
        <div class="col-md-6 mb-2">
          <div class="form-group">
            <label for="price">Select Product</label>
            <select class="form-control form-control-lg form-control-a" id="price">
              <option>Select</option>
              <option>Chemical</option>
              <option>Polymer</option>
              <option>Metal</option>
              <option>Paper</option>
            </select>
          </div>
        </div>
        <div class="col-md-12">
          <button type="submit" class="btn btn-b">Search Product</button>
        </div>
      </div>
    </form>
  </div>
</div>
<!--/ Form Search End /-->

<!--/ Nav Star /-->
<nav class="navbar navbar-default navbar-trans navbar-expand-lg fixed-top">
    <div class="container">
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
        aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <a class="navbar-brand text-brand" href="/">B2B <span class="color-b">Seller</span></a>
      <button type="button" class="btn btn-link nav-search navbar-toggle-box-collapse d-md-none" data-toggle="collapse"
        data-target="#navbarTogglerDemo01" aria-expanded="false">
        <span class="fa fa-search" aria-hidden="true"></span>
      </button>
      <div class="navbar-collapse collapse justify-content-center" id="navbarDefault">
        <ul class="navbar-nav">
            {{-- <li class="nav-item">
                <a class="nav-link" href="blog-grid.html">Blog</a>
            </li> --}}
        @auth

            {{-- Only user with user role can become a seller --}}
            @can('user')
              @empty(Auth::user()->sellerDetails)
                <li class="nav-item">
                  <a class="nav-link {{ request()->is('seller/*') ? 'active' : '' }}" href="{{ route('seller.request') }}">Become a Seller</a>
                </li>
              @endempty
            @endcan

            @can('seller')
              <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#add-products" href="#">Add New Product</a>
              </li>
            @endcan

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-user"></i> 
              {{ ucfirst(Auth::user()->name) }}
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                @can('admin')
                    <a class="dropdown-item" href="/dashboard">View Dashboard</a>
                @endcan
                
                @cannot('admin')
                    <a class="dropdown-item" href="{{ route('profile.index') }}">My Profile</a>
                @endcannot
              
              <a class="dropdown-item" href="{{ route('logout') }}"
              onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            Logout
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              </a>
            </div>
          </li>
        @endauth
          
          @guest
            <li class="nav-item">
                <a class="nav-link link1">Login / Register</a>
            </li>
          @endguest
          
        </ul>
      </div>
      <button type="button" class="btn btn-b-n navbar-toggle-box-collapse d-none d-md-block" data-toggle="collapse"
        data-target="#navbarTogglerDemo01" aria-expanded="false">
        <span class="fa fa-search" aria-hidden="true"></span>
      </button>
    </div>
  </nav>
  <!--/ Nav End /-->