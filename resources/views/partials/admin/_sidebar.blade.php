<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('img/user.png') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ ucfirst(Auth::user()->name) }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li><a class="{{ request()->is('dashboard') ? 'active' : '' }}" href="{{ route('admin.index') }}"> <i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

        <li><a class="{{ request()->is('dashboard/users') ? 'active' : '' }}" href="{{ route('admin.users.index') }}"> <i class="fa fa-user"></i> <span>Users</span></a></li>

        <li><a class="{{ request()->is('dashboard/seller-requests') ? 'active' : '' }}" href="{{ route('admin.seller.index') }}"> <i class="fa fa-user-plus"></i> <span>Seller Requests</span></a></li>

        <li><a class="{{ request()->is('dashboard/categories') ? 'active' : '' }}" href="{{ route('admin.categories.index') }}"> <i class="fa fa-bookmark-o"></i> <span>Categories</span></a></li>
        
        {{-- <li class="active treeview menu-open">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li class="active"><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li> --}}
        
        <li><a href="#"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>