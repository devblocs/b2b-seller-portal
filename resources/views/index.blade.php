@extends('layouts.user')

@php
   if(Auth::check()){
       $msg = "Welcome to Ezrics! | " . ucfirst(Auth::user()->name);
   }else{
        $msg = "Welcome to Ezrics!";
   }
@endphp

@section('title', $msg)

@section('content')

<!--/ LOGIN SIGNUP MODAL START /-->
<div class="backdrop"></div>
  @guest
  <div class="custom-modal">
        <div class="login-box">
            <div class="lb-header">
            <a href="#" class="active" id="login-box-link">LOGIN</a>
            <a href="#" id="signup-box-link">SIGN UP</a>
            </div>
            {{-- Login form --}}
            <form class="email-login form-a contactForm padding-1" method="POST" action="{{ route('login') }}">
              @csrf
              <div class="row">
                  <div class="col-md-12 mb-1">
                      <div class="form-group margin-1">
                        <input type="email" class="form-control form-control-lg form-control-a {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Enter Your Registered Email">
                          @if ($errors->has('email'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>
                  <div class="col-md-12 mb-1 margin-1">
                      <div class="form-group">
                        <input type="password" name="password" class="form-control form-control-lg form-control-a {{ $errors->has('password') ? ' is-invalid' : '' }}" required placeholder="Enter Your Password">
                          @if ($errors->has('password'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('password') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>
                  <div class="col-md-12 mb-1 margin-1">
                      <div class="form-group">
                          <div class="form-check">
                              <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
  
                              <label class="form-check-label" for="remember">
                                  {{ __('Remember Me') }}
                              </label>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-12">
                      <button type="submit" class="btn btn btn-a">
                              {{ __('Login') }}
                          </button>
  
                          @if (Route::has('password.request'))
                              <a class="btn btn-link" href="{{ route('password.request') }}">
                                  {{ __('Forgot Your Password?') }}
                              </a>
                          @endif
                  </div>
              </div>
            </form>

            {{-- Register form --}}
            <form class="email-signup form-a contactForm padding-1" method="POST" action="{{ route('register') }}">
                @csrf
                <div class="row">
                    <div class="col-md-12 mb-1">
                        <div class="form-group margin-1">
                          <input type="text" name="name" class="form-control form-control-lg form-control-a {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}" required autofocus placeholder="Your Full Name">

                            @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12 mb-1">
                        <div class="form-group margin-1">
                            <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">+91</div>
                                    </div>
                                    <input type="text" class="form-control form-control-lg form-control-a{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" value="{{ old('mobile') }}" required placeholder="Your Mobile Number">
                            </div>
                            @if ($errors->has('mobile'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('mobile') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12 mb-1">
                        <div class="form-group margin-1">
                          <input type="email" name="email" class="form-control form-control-lg form-control-a{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="Your Email" required>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12 mb-1 margin-1">
                        <div class="form-group">
                          <input type="password" class="form-control form-control-lg form-control-a{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12 mb-1">
                        <div class="form-group margin-1">
                        <input id="password-confirm" type="password" class="form-control form-control-lg form-control-a" name="password_confirmation" placeholder="Confirm Password" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-a">Sign Up</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
  @endguest
  <!--/ LOGIN SIGNUP MODAL END /--> 

 

<!--/ Services Star /-->
<section class="section-services section-t8">
  <div class="container">
    
    <div class="row  services-row">
      <div class="col-md-4">
        <div class="card-box-c foo">
          <div class="card-header-c d-flex">
            
            <div class="card-title-c align-self-center">
              <h2 class="title-c">Chemicals</h2>
            </div>
          </div>
          <div class="card-body-c">
            <div class="content-c">
                <ul class="list-unstyled">
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product Y is Offered at Rs.35 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                  </ul>
            </div>
          </div>
          <div class="card-footer-c">
            <a href="product-menu.html" class="link-c link-icon">View All
              <span class="ion-ios-arrow-forward"></span>
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card-box-c foo">
          <div class="card-header-c d-flex">
            
            <div class="card-title-c align-self-center">
              <h2 class="title-c">Polymers</h2>
            </div>
          </div>
          <div class="card-body-c">
            <div class="content-c">
                <ul class="list-unstyled">
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product Y is Offered at Rs.35 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                  </ul>
            </div>
          </div>
          <div class="card-footer-c">
            <a href="product-menu.html" class="link-c link-icon">View All
              <span class="ion-ios-arrow-forward"></span>
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card-box-c foo">
          <div class="card-header-c d-flex">
            
            <div class="card-title-c align-self-center">
              <h2 class="title-c">Sugar</h2>
            </div>
          </div>
          <div class="card-body-c">
            <div class="content-c">
                <ul class="list-unstyled">
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product Y is Offered at Rs.35 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                  </ul>
            </div>
          </div>
          <div class="card-footer-c">
            <a href="product-menu.html" class="link-c link-icon">View All
              <span class="ion-ios-arrow-forward"></span>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row services-row">
      <div class="col-md-4">
        <div class="card-box-c foo">
          <div class="card-header-c d-flex">
            
            <div class="card-title-c align-self-center">
              <h2 class="title-c">Metals</h2>
            </div>
          </div>
          <div class="card-body-c">
            <div class="content-c">
                <ul class="list-unstyled">
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product Y is Offered at Rs.35 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                  </ul>
            </div>
          </div>
          <div class="card-footer-c">
            <a href="product-menu.html" class="link-c link-icon">View All
              <span class="ion-ios-arrow-forward"></span>
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card-box-c foo">
          <div class="card-header-c d-flex">
            
            <div class="card-title-c align-self-center">
              <h2 class="title-c">Papers</h2>
            </div>
          </div>
          <div class="card-body-c">
            <div class="content-c">
                <ul class="list-unstyled">
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product Y is Offered at Rs.35 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                  </ul>
            </div>
          </div>
          <div class="card-footer-c">
            <a href="product-menu.html" class="link-c link-icon">View All
              <span class="ion-ios-arrow-forward"></span>
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card-box-c foo">
          <div class="card-header-c d-flex">
            
            <div class="card-title-c align-self-center">
              <h2 class="title-c">Reseller</h2>
            </div>
          </div>
          <div class="card-body-c">
            <div class="content-c">
                <ul class="list-unstyled">
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product Y is Offered at Rs.35 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                    <li class="color-text-a"><a href="product-detail.html">Product X is Offered at Rs.25 / kg</a></li>
                  </ul>
            </div>
          </div>
          <div class="card-footer-c">
            <a href="product-menu.html" class="link-c link-icon">View All
              <span class="ion-ios-arrow-forward"></span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--/ Services End /-->
@endsection